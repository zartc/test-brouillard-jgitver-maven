#!/usr/bin/env bash

# First of all, copy all required files: .mvn, .gitignore, pom.xml, readme.adoc, template-git-project.sh (this script)
# into the project directory, then cd to the project directory and execute this script.
# After that, play with the git history to see how jgitver will compute the artifact version in diffrent conditions.

git init

git add .
git commit -m "initial commit"

echo "M1" >> readme.adoc
git commit -am "add M1"
echo "M2" >> readme.adoc
git commit -am "add M2"
git tag -am "v1.0" v1.0   # annotated tag

git checkout -b "feature/barcelona"
echo "B1" >> readme.adoc
git commit -am "add B1"
echo "B2" >> readme.adoc
git commit -am "add B2"
git tag -am "v1.1" v1.1 # annotated tag
echo "B3" >> readme.adoc
git commit -am "add B3"

git checkout master -b "feature/copenhagen"
echo "C1" >> readme.adoc
git tag v2.0 # not annotated tag (intentional)
git commit -am "add C1"
echo "C2" >> readme.adoc
git commit -am "add C2"

git checkout master
echo "M3" >> readme.adoc
git commit -am "add M3"
